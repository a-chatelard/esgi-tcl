# EsgiTcl

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.4.

## Development server

Pour faire fonctionner l'ensemble des éléments nécessaires au bon fonctionnement du l'application. Merci d'executer ces commandes :
- `npm install`
- `npm run mock:server`
- `ng serve --open`

L'application s'appuie sur le package json-server afin d'imiter la récupération de données depuis une API mise à disposition.
Vous pouvez retrouver les données récupérées dans le fichier mocks/data.json et les modifier si besoin.


## Graphe 

Le graphe utilisé reprend les lignes de métro TCL de Lyon auxquelles une durée arbitraire entre chaque arrêt a été définie.
Le plan a été légèrement modifié afin de présenter des cas de tests utiles (tels que la jointure Gorge de Loup - Saint-Just ou Masséna - Croix-Rousse).

Vous trouverez également dans le dossier images, [une image du plan où les temps entre chaque arrêt sont indiqués](./src/assets/images/planMetroTCL-Temps.png) afin d'aider dans les tests.

Voici l'ensemble des arrêts ainsi que leur identifiants :
|Id|Libellé|
|---|---|
|1| Gare Part-Dieu
|2| Place Guichard
|3| Saxe-Gambetta
|4| Garibaldi
|5| Jean-Macé
|6| Place Jean Jaurès
|7| Debourg
|8| Stade de Gerland
|9| Gare d'Oullins
|10| Guillotière
|11| Bellecour
|12| Vieux Lyon
|13| Minimes
|14| Saint-Just
|15| Gorge de Loup
|16| Valmy
|17| Gare de Vaise
|18| Fourvière
|19| Cordeliers
|20| Hôtel de Ville
|21| Croix-Paquet
|22| Croix-Rousse
|23| Hénon
|24| Cuire
|25| Foch
|26| Masséna
|27| Charpennes
|28| Brotteaux
|29| République
|30| Gratte-Ciel
|31| Flachet
|32| Cusset
|33| Vault-en-Velin - La Soie
|34| Ampère
|35| Perrache

## Comparaison de performances

Il est complexe de comparer la réelle consommation en termes de performances de chaque algorithme de parcours du graphe pour trouver le chemin le plus rapide ou le plus facile entre deux points.
Il faudrait s'appuyer sur des graphes bien plus conséquents pour faire apparaitre les avantages et inconvénients de chacune des méthodes parcours.
Cependant, en considérant le fonctionnement des deux parcours, on peut conclure que l'algorithme de Dijktra reste généralement plus coûteux en ressources que le parcours en largeur. En effet celui-ci doit parcourir l'ensemble des sommets du graphe afin de déterminer le chemin le plus rapide entre deux sommets. Tandis que le parcours en largeur s'arrête dès qu'il atteint la destination souhaitée.
De ce fait, l'algorithme de Dijktra engendre plus d'interactions avec les objets représentant le graphe donc nécessite plus de ressources.
