import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';

const declaredModules = [
  MatSelectModule,
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatIconModule
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    declaredModules
  ],
  exports: [
    declaredModules
  ]
})
export class MaterialModule { }
