import { Injectable } from '@angular/core';
import { Adjacent } from '../../models/adjacent';
import { Branche } from '../../models/branche';
import { Sommet } from '../../models/sommet';
import { Noeud } from '../../models/noeudDijkstra';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class GraphBuilderService {
  test: Noeud[];

  getNoeudsDijkstras(): Noeud[] {
    this.api.getSommets().subscribe((sommets: Sommet[]) => {
      sommets.forEach(sommet => {
        this.test.push({ id: sommet.id, label: sommet.libelle, visited: false, distanceFromSource: Infinity, children: [], bestParentFromSource: null });
      });
      this.api.getAdjacents().subscribe((adjacents: Adjacent[]) => {
        adjacents.forEach(adjacent => {
          let destination = this.test.find(n => n.id == adjacent.destination);
          let branche: Branche = { destination: destination!, poids: adjacent.temps };
          let depart = this.test.find(n => n.id == adjacent.depart);
          depart!.children.push(branche);
        });
      });
    });
    return this.test;
  }

  resetDijkstras(noeuds: Noeud[]) {
    noeuds.forEach(noeud => {
      noeud.bestParentFromSource = null;
      noeud.distanceFromSource = Infinity;
      noeud.visited = false;
    });
  } 
  

  constructor(private api: ApiService) {
    this.test = [];
   }
}
