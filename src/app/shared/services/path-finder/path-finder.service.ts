import { Injectable } from '@angular/core';
import { Noeud } from '../../models/noeudDijkstra';

@Injectable({
  providedIn: 'root'
})
export class PathFinderService {

  constructor() { }

  // Représente le parcours utilisant l'algorithme de dijkstra.
  buildDijkstrasFrom(dijkstras: Noeud[], currentNode: Noeud | null) {
    while (currentNode != null) {
      currentNode.visited = true;
      currentNode.children.forEach(branche => {
        if (branche.destination.distanceFromSource > currentNode!.distanceFromSource + branche.poids) {
          branche.destination.distanceFromSource = currentNode!.distanceFromSource + branche.poids;
          branche.destination.bestParentFromSource = currentNode;
        }
      });
      let notVisitedDijkstras = dijkstras.filter(d => d.visited == false);
      if (notVisitedDijkstras.length == 0) {
        currentNode = null;
      } else {
        currentNode = notVisitedDijkstras.reduce((prev, curr) => {
          return prev.distanceFromSource < curr.distanceFromSource ? prev : curr;
        });
      }
    }
  }

  getPathTo(currentNode: Noeud | null) {
    let path: Noeud[] = [];
    while(currentNode?.distanceFromSource != 0){
      path.unshift(currentNode!);
      currentNode = currentNode!.bestParentFromSource;
    } 
    path.unshift(currentNode);
    return path;
  }

  // Représente le parcours en largeur.
  findShortestPathFromTo(start: Noeud, end: Noeud) {
    let queue: Noeud[] = [];
    let found = false;
    start.distanceFromSource = 0;
    queue.push(start);
    while (queue.length != 0 || found == false) {
      let currentNode: Noeud = queue.shift()!;
      if (!currentNode.visited) {
        currentNode.visited = true;
        if (currentNode == end) {
          found = true;
          break;
        }
        currentNode.children.forEach(child => {
          if (!child.destination.visited) {
            child.destination.bestParentFromSource = currentNode;
            child.destination.distanceFromSource = currentNode.distanceFromSource + child.poids;
            queue.push(child.destination);
          }
        });
      }
    }
  }
}
