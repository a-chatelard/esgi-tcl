import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Adjacent } from '../../models/adjacent';
import { Sommet } from '../../models/sommet';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  getSommets(): Observable<Sommet[]> {
    return this.httpClient.get<Sommet[]>("http://localhost:3000/noeuds");
  }

  getAdjacents(): Observable<Adjacent[]> {
    return this.httpClient.get<Adjacent[]>("http://localhost:3000/liens");
  }

  constructor(private httpClient: HttpClient) { }
}