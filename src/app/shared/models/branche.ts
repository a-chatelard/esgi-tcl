import { Noeud } from "./noeudDijkstra";

export interface Branche {
  poids: number;
  destination: Noeud;
}