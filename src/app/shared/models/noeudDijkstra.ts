
import { Branche } from "./branche";

export interface Noeud {
  id: number;
  label: string;
  children: Branche[];
  visited: boolean;
  distanceFromSource: number;
  bestParentFromSource: Noeud | null;
}