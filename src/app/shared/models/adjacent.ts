export interface Adjacent {
  depart: number;
  destination: number;
  temps: number;
}