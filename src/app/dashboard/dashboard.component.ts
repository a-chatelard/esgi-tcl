import { Time } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Noeud } from '../shared/models/noeudDijkstra';
import { GraphBuilderService } from '../shared/services/graph-builder/graph-builder.service';
import { PathFinderService } from '../shared/services/path-finder/path-finder.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [GraphBuilderService]
})
export class DashboardComponent implements OnInit {
  selectedDepart?: number;
  selectedDestination?: number;
  oldSelectedDepart?: Noeud;
  noeuds: Noeud[];
  quickestPath: any[];
  shortestPath: any[];
  dijkstrasTime?: number;
  lengthTime?: number;


  constructor(
    private graphBuilder: GraphBuilderService, 
    private pathfinder: PathFinderService) {
    this.noeuds = this.graphBuilder.getNoeudsDijkstras();
    this.quickestPath = [];
    this.shortestPath = [];
    this.dijkstrasTime = undefined;
    this.lengthTime = undefined;
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.selectedDepart != undefined && this.selectedDestination != undefined) {
      let depart = this.noeuds.find(noeud => noeud.id == this.selectedDepart);
      this.graphBuilder.resetDijkstras(this.noeuds);
      depart!.distanceFromSource = 0;

      let startTime = Date.now();
      console.log(startTime);

      // Parcours dijktra
      this.pathfinder.buildDijkstrasFrom(this.noeuds, depart!);
      let destination = this.noeuds.find(noeud => noeud.id == this.selectedDestination);
      this.quickestPath = this.pathfinder
        .getPathTo(destination!)
        .map(noeud => ({label: noeud.label, distanceFromSource: noeud.distanceFromSource}));
      this.graphBuilder.resetDijkstras(this.noeuds);

      this.dijkstrasTime = Date.now() - startTime;
      startTime = Date.now();
      console.log(startTime);

      // Parcours en largeur
      this.pathfinder.findShortestPathFromTo(depart!, destination!);
      this.shortestPath = this.pathfinder
        .getPathTo(destination!)
        .map(noeud => ({label: noeud.label, distanceFromSource: noeud.distanceFromSource}));

      this.lengthTime = Date.now() - startTime;
    }
  }

  newItineraire() {
    this.quickestPath = [];
    this.shortestPath = [];
  }

  arePathsEquals() {
    let simpleQuickestPath = this.quickestPath.map(e => (e.id));
    let simpleShortestPath = this.shortestPath.map(e => (e.id));
    return (
      simpleQuickestPath.length === simpleShortestPath.length &&
      simpleQuickestPath.every((value, index) => value === simpleShortestPath[index])
    );
  }
}
